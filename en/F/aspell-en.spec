%define lang en
%define langrelease 0
Summary: English dictionaries for Aspell.
Name: aspell-%{lang}
Version: 0.51
Release: 6.ker.rhel3
License: distributable
Group: Applications/Text
URL: http://aspell.net/
Source: ftp://ftp.gnu.org/gnu/aspell/dict/%{lang}/aspell-%{lang}-%{version}-%{langrelease}.tar.bz2
Buildrequires: aspell >= 0.50
Requires: aspell >= 0.50
BuildRoot: %{_tmppath}/%{name}-%{version}-root

%description
Provides the word list/dictionaries for the following: English, Canadian
English, British English

%prep
rm -rf $RPM_BUILD_ROOT
%setup -q -n aspell-%{lang}-%{version}-%{langrelease}

%build
echo "ASPELL = aspell" > Makefile
echo "DEST_DIR = $RPM_BUILD_ROOT" >> Makefile
echo "WORD_LIST_COMPRESS = word-list-compress" >> Makefile
echo "dictdir = ${RPM_BUILD_ROOT}%{_libdir}/aspell" >> Makefile
echo "datadir = ${RPM_BUILD_ROOT}%{_datadir}/aspell" >> Makefile
cat Makefile.pre >> Makefile
make

%install
make install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)

%{_libdir}/aspell/*
%{_datadir}/aspell/*

%changelog
* Sat Feb 14 2004 Milan Kerslager <kerslage@linux.cz> 0.51-6.ker.rhel3
- rebuild for RHEL3
- package is from FC1

* Mon Jun 23 2003 Adrian Havill <havill@redhat.com> 0.51-6
- data files are not arch independent

* Fri Jun 20 2003 Adrian Havill <havill@redhat.com> 0.51-5
- first build for new aspell (0.50)
